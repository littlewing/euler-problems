package problem7;

/**
 * User: touret-a
 * Date: 04/12/12
 * Time: 13:08
 */
public class Problem7Main {
    public static void main(String... args ){
        int primenumberIndex=6;
        int number=14;
        int lastPrimeNumber=13;
        int primenumberLength=10001;
        while(primenumberIndex<primenumberLength){
            if(isPrime(number)){
                lastPrimeNumber=number;
                primenumberIndex++;
                System.out.println("Nombre premier ["+primenumberIndex+"] = "+lastPrimeNumber);
            }
            number++;
        }
        System.out.println("10001 nombre premier:"+lastPrimeNumber);
    }


    public static boolean isPrime(int number){
        boolean isPrime = true;
        if(number%2==0){
            isPrime=false;
        }
        else{
            for(int i=3;i<number;i+=2){
                if(number%i==0){
                    isPrime=false;
                    break;
                }
            }
        }
        return isPrime;
    }
}
