package problem1;

import java.math.BigDecimal;

/**
 * User: touret-a
 * Date: 26/11/12
 * Time: 09:48
 */
public class Problem1Main {
    public static void main(String... args) {
        int sum = 0;
        for (float i = 1; i < 1000; i++) {
            if (i%3 ==0||i%5==0) {
                System.out.println("Nombre trouve multiple de 3 ou 5:"+i);
                sum += i;
            }
        }
        System.out.println("SOMME : "+sum);
    }
}
