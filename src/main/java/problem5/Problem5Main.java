package problem5;

/**
 * User: touret-a
 * Date: 04/12/12
 * Time: 09:09
 */
public class Problem5Main {

    public final static void main(String... args ){
        boolean isDivisibleByAll = false;

        int number=21;
        while (!isDivisibleByAll){
            for(int i=1;i<=20;i++){
                if(number % i ==0 ){
                    isDivisibleByAll=true;
                }else{
                    isDivisibleByAll=false;
                    number++;
                    break;
                }
            }
        }
        System.out.println("Nombre divisible de 1 -> 20 = "+number);
    }

}
