package problem9;

/**
 * User: touret-a
 * Date: 04/12/12
 * Time: 13:50
 */
public class Problem9Main {
    public static void main(String... args) {
        double tripletsum = 0;
        double a = 1;
        double b = 2;
        double c = 0;

        loopA:for (a = 1; a < 1000; a++) {
            loopB:for (b = a+1; b < 1000 - a; b++) {
                c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
                if (a + b + c == 1000 ) {
                    System.out.println("------------------------------------------- ");
                    System.out.println("A =" + a + " / B =" + b + " /C =" + c + " a*b*c= " + a * b * c);
                    System.out.println("------------------------------------------- ");
                    break loopA;
                }
            }
        }
    }
}
