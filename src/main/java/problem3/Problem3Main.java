package problem3;

/**
 * User: touret-a
 * Date: 28/11/12
 * Time: 16:45
 */
public class Problem3Main {

    private final static long NUMBER_TO_EXECUTE = 600851475143L;

    public static void main(String... args) {
        Problem3Main problem3Main = new Problem3Main();
        // Algorithme euclide
        //System.out.println("RESULTAT = "+problem3Main.getPrimeFactor(NUMBER_TO_EXECUTE,5L));
        long primefactor = 2;
        long index = 2L;
        long max = NUMBER_TO_EXECUTE;
        while (index < max) {
            while (max % index != 0) {
                index++;

            }
            System.out.println("primefactor trouve :"+primefactor);
            primefactor = index;
            max = max / primefactor;
            index++;
        }
        System.out.println("primefactor max :"+primefactor);
    }
}
