package problem2;

/**
 * User: touret-a
 * Date: 28/11/12
 * Time: 11:13
 */
public class Problem2Main {
    private final static int MAX = 4000000;

    public static void main(String... args) {
        System.out.println("Démarrage back to the future...");
        int sum = 0;

        Problem2Main problem2Main = new Problem2Main();
        sum = problem2Main.getSumFibonnaci();
        System.out.println("SOMME=" + sum);
    }

    public Fibonnaci fibonacci(Fibonnaci _fibonnaci) {
        Fibonnaci fibonacci = new Fibonnaci(1, 2,2);
        System.out.println("[" + _fibonnaci.getValue1() + "] [" + _fibonnaci.getValue2() + "] ==> [" + _fibonnaci.getEvenNumberSum() + "]");
        int sum = _fibonnaci.getValue1() + _fibonnaci.getValue2();
        if (_fibonnaci.getValue1() + _fibonnaci.getValue2() < MAX) {
            _fibonnaci.setValue1(_fibonnaci.getValue2());
            _fibonnaci.setValue2(sum);
            if (_fibonnaci.getValue2() % 2 == 0) {
                _fibonnaci.setEvenNumberSum(_fibonnaci.getEvenNumberSum() + _fibonnaci.getValue2());
            }
            fibonacci = fibonacci(_fibonnaci);
        } else {
            fibonacci = _fibonnaci;
        }
        return fibonacci;
    }

    public int getSumFibonnaci() {
        return fibonacci(new Fibonnaci(1, 2)).getEvenNumberSum();
    }

    class Fibonnaci {
        private int value1;
        private int value2;
        private int evenNumberSum = 0;

        public int getValue1() {
            return value1;
        }

        public void setValue1(int value1) {
            this.value1 = value1;
        }

        public int getValue2() {
            return value2;
        }

        public void setValue2(int value2) {
            this.value2 = value2;
        }

        public int getEvenNumberSum() {
            return evenNumberSum;
        }

        public void setEvenNumberSum(int evenNumberSum) {
            this.evenNumberSum = evenNumberSum;
        }

        Fibonnaci(int value1, int value2) {
            this.value1 = value1;
            this.value2 = value2;
            evenNumberSum = 2;
        }
        Fibonnaci(int value1, int value2,int sum) {
            this.value1 = value1;
            this.value2 = value2;
            evenNumberSum = sum;
        }

    }

}
