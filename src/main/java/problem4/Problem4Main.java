package problem4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: touret-a
 * Date: 28/11/12
 * Time: 17:59
 */
public class Problem4Main {

    public final static void main(String... args) {
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 100; i < 1000; i++) {
            for (int j = 100; j < 1000; j++) {
                final int number = i * j;
                final String value = String.valueOf(number);
                final double length = value.length();
                boolean isPalindrome = true;
                for (int k = 0; k < (int) Math.floor(length / 2); k++) {
                    isPalindrome = (isPalindrome && (value.charAt(k) == value.charAt((int) length - k - 1)));
                    if (!isPalindrome) {
                        break;
                    }
                }
                if (isPalindrome) {
                    list.add(number);
                }
            }
        }
        Collections.sort(list);
        System.out.println("PALINDROME  MAX  = " + list.get(list.size() - 1));
    }
}
